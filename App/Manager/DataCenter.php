<?php
/**
 * Created by PhpStorm
 * author  :gogochen
 * date    :2019/12/13
 * time    :14:48
 */

namespace App\Manager;


use App\Lib\Redis;

/**
 * description  游戏管理
 * Class DataCenter
 * @package App\Manager
 */
class DataCenter
{
    //redis的key前缀
    const PREFIX_KEY = "game";
    //存储对战房的数据
    public static $global;

    public static $server;

    /**
     * description  格式化输出日志
     * @param $info
     * @param array $context
     * @param string $level
     */
    public static function log($info, $context = [], $level = 'INFO')
    {
        if (!empty($context)) {
            echo sprintf(
                "[%s][%s]: %s %s" . PHP_EOL,
                date('Y-m-d H:i:s'),
                $level,
                $info,
                json_encode($context, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            );
        } else {
            echo sprintf(
                "[%s][%s]: %s" . PHP_EOL,
                date('Y-m-d H:i:s'),
                $level,
                $info
            );
        }
    }


    public static function redis()
    {
        return Redis::getInstance();
    }


    /**
     * description   获取redis队列的长度(玩家队列）
     * @return bool|int
     */
    public static function getPlayerWaitListLen()
    {
        $key = self::PREFIX_KEY . ":player_wait_list";
        return self::redis()->lLen($key);
    }

    /**
     * description  入列。玩家加入等待队列直到人数达到要求
     * @param $playerId
     */
    public static function pushPlayerToWaitList($playerId)
    {
        $key = self::PREFIX_KEY . ":player_wait_list";
        self::redis()->lPush($key, $playerId);
    }

    /**
     * description  出列，玩家出列
     * @return bool|mixed
     */
    public static function popPlayerFromWaitList()
    {
        $key = self::PREFIX_KEY . ":player_wait_list";
        return self::redis()->lPop($key);
    }

    /**
     * description   根据$playerId  获取fd
     * @param $playerId
     * @return bool|mixed|string
     */
    public static function getPlayerFd($playerId)
    {
        $key = self::PREFIX_KEY . ":player_fd:" . $playerId;
        return self::redis()->get($key);
    }

    /**
     * description   把fd存进redis
     * @param $playerId
     * @param $playerFd
     */
    public static function setPlayerFd($playerId, $playerFd)
    {
        $key = self::PREFIX_KEY . ":player_fd:" . $playerId;
        self::redis()->set($key, $playerFd);
    }

    /**
     * description  销毁fd
     * @param $playerId
     */
    public static function delPlayerFd($playerId)
    {
        $key = self::PREFIX_KEY . ":player_fd:" . $playerId;
        self::redis()->del($key);
    }


    /**
     * description  根据fd获取playerId
     * @param $playerFd
     * @return bool|mixed|string
     */
    public static function getPlayerId($playerFd)
    {
        $key = self::PREFIX_KEY . ":player_id:" . $playerFd;
        return self::redis()->get($key);
    }

    /**
     * description   playerId存进redis
     * @param $playerFd
     * @param $playerId
     */
    public static function setPlayerId($playerFd, $playerId)
    {
        $key = self::PREFIX_KEY . ":player_id:" . $playerFd;
        self::redis()->set($key, $playerId);
    }

    /**
     * description  销毁playerId
     * @param $playerFd
     */
    public static function delPlayerId($playerFd)
    {
        $key = self::PREFIX_KEY . ":player_id:" . $playerFd;
        self::redis()->del($key);
    }

    /**
     * description  保存playerId，fd
     * @param $playerId
     * @param $playerFd
     */
    public static function setPlayerInfo($playerId, $playerFd)
    {
        self::setPlayerId($playerFd, $playerId);
        self::setPlayerFd($playerId, $playerFd);
    }

    /**
     * description   销毁playerId,fd
     * @param $playerFd
     */
    public static function delPlayerInfo($playerFd)
    {
        $playerId = self::getPlayerId($playerFd);
        self::delPlayerId($playerFd);
        self::delPlayerFd($playerId);
    }

    /**
     * description  清除redis中的残留内容
     */
    public static function initDataCenter()
    {
        //清空匹配队列
        $key = self::PREFIX_KEY . ':player_wait_list';
        self::redis()->del($key);
        //清空玩家id
        $key = self::PREFIX_KEY . ':player_id*';
        $values = self::redis()->keys($key);
        foreach ($values as $value){
            self::redis()->del($value);
        }
        //清空玩家fd
        $key = self::PREFIX_KEY.':player_fd*';
        $values = self::redis()->keys($key);
        foreach ($values as $value){
            self::redis()->del($value);
        }
    }


}