<?php
/**
 * Created by PhpStorm
 * author  :gogochen
 * date    :2019/12/12
 * time    :11:12
 */

namespace App\Manager;


use App\Model\Map;
use App\Model\Player;

/**
 * description  游戏控制
 * Class Game
 * @package App\Manager
 */
class Game
{
    //定义一个对象保存地图数据
    private $gameMap = [];

    //定义一个数组保存两个玩家
    private $players = [];


    public function __construct()
    {
        $this->gameMap = new Map(12, 12);
    }

    /**
     * description  创建玩家
     * @param $playerId
     * @param $x
     * @param $y
     */
    public function createPlayer($playerId, $x, $y)
    {
        $player = new Player($playerId, $x, $y);
        if (!empty($this->players)) {
            $player->setType(Player::PLAYER_TYPE_HIDE);
        }
        $this->players[$playerId] = $player;
    }

    public function getPlayers()
    {
        return $this->players;
    }


    public function getMapData()
    {
        return $this->gameMap->getMapData();
    }

    /**
     * description  玩家移动
     * @param $playerId
     * @param $direction
     */
    public function playerMove($playerId, $direction)
    {
        $player = $this->players[$playerId];
        if ($this->canMoveToDirection($player, $direction)) {
            $player->{$direction}();
        }
    }

    public function printGameMap()
    {
        $mapData = $this->gameMap->getMapData();
        $font = [2 => '追', 3 => '躲'];
        /* @var Player $player */
        foreach ($this->players as $player) {
            $mapData[$player->getX()][$player->getY()] = $player->getType() + 1;
        }

        foreach ($mapData as $line) {
            foreach ($line as $item) {
                if (empty($item)) {
                    echo "墙，";
                } elseif ($item == 1) {
                    echo "    ";
                } else {
                    echo $font[$item] . '，';
                }
            }
            echo PHP_EOL;
        }
    }

    /**
     * description  获取方向，计算得出目标坐标
     * @param Player $player
     * @param $direction
     * @return bool
     */
    private function canMoveToDirection($player, $direction)
    {
        $x = $player->getX();
        $y = $player->getY();
        $moveCoor = $this->getMoveCoor($x, $y, $direction);

        $mapData = $this->gameMap->getMapData();
        //检测目标坐标能不能走，所以当数组中是0的时候就返回false，否则返回true
        if (!$mapData[$moveCoor[0]][$moveCoor[1]]) {
            return false;
        }
        return true;
    }

    /**
     * description   移动
     * @param $x
     * @param $y
     * @param $direction
     * @return array
     */
    private function getMoveCoor($x, $y, $direction)
    {
        switch ($direction) {
            case Player::UP:
                return [--$x, $y];
            case Player::DOWN:
                return [++$x, $y];
            case Player::LEFT:
                return [$x, --$y];
            case  Player::RIGHT:
                return [$x, ++$y];
        }
        return [$x, $y];
    }


    public function isGameOver()
    {
        $result = false;
        $x = -1;
        $y = -1;
        $players = array_values($this->players);

        foreach ($players as $key => $player) {
            if ($key == 0) {
                $x = $player->getX();
                $y = $player->getY();
            } elseif ($x == $player->getX() and $y == $player->getY()) {
                return true;
            }
        }
        return $result;
    }
}